const isTest = String(process.env.NODE_ENV) === 'test';

module.exports = {
  plugins: [['@babel/plugin-proposal-class-properties', { loose: true }]],
  env: {
    production: {
      presets: ['minify'],
    },
  },
  presets: [
    '@babel/preset-react',
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: ['last 2 versions', 'safari >= 9', 'ie >= 11'],
        },
        loose: true,
        modules: isTest ? 'commonjs' : false,
      },
    ],
    ['@emotion/babel-preset-css-prop'],
  ],
};
