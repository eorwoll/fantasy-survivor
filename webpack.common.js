const path = require('path');
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const cleanOptions = {
  exclude: ['normalize.css'],
  verbose: true,
};

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env',
                {
                  plugins: ['@babel/plugin-proposal-class-properties'],
                },
              ],
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            // Creates style nodes from JS strings
            loader: 'style-loader',
          },
          {
            // Translates CSS into CommonJS
            loader: 'css-loader',
          },
          {
            // Run post css actions
            loader: 'postcss-loader',
            options: {
              plugins: function() {
                // Post css plugins, can be exported to postcss.config.js
                return [autoprefixer];
              },
            },
          },
          {
            // Compiles Sass to CSS
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin('dist', cleanOptions),
    new SimpleProgressWebpackPlugin(),
  ],
};
