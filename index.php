<!DOCTYPE html>
<html lang="en" ng-app="fs">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <title>Fantasy Survivor</title>

      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
      <link rel="icon" href="/favicon.ico" type="image/x-icon">
      <!--<link href="css/custom.css" rel="stylesheet">-->
      <link href='http://fonts.googleapis.com/css?family=Bowlby+One+SC|Roboto:400,300' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="css/global.css">
      
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]><link href= "css/bootstrap-theme.css"rel= "stylesheet" >

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  </head>

  <body ng-controller="ApplicationController">
    <div class="container">

      <div ui-view autoscroll="false"></div>
      
    </div>
    
    <div login-dialog ng-if="!isLoginPage"></div>
  </body>
  <!-- Libs -->
  
  <script src="js/jquery/dist/jquery.min.js"></script>
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
  <script type="text/javascript" src="app/tinymce.js"></script>
  <script type="text/javascript" src="js/angular-ui-router.min.js"></script>
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-sanitize.js"></script>
  <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-cookies.js"></script>-->
  <script type="text/javascript" src="app/app.js"></script>
  <script type="text/javascript" src="app/utilities.js"></script>
  <script type="text/javascript" src="app/admin/SurvivorsController.js"></script>
  <script type="text/javascript" src="app/admin/UsersController.js"></script>
  <script type="text/javascript" src="app/admin/PointsController.js"></script>
  <script type="text/javascript" src="app/admin/TribesController.js"></script>
  <script type="text/javascript" src="app/admin/admin.js"></script>
  <script type="text/javascript" src="app/DashboardController.js"></script>
  <script type="text/javascript" src="app/StandingsController.js"></script>
  <script type="text/javascript" src="app/SurvivorInfoController.js"></script>
  <script type="text/javascript" src="app/EpisodesController.js"></script>
  <script type="text/javascript" src="app/ProfileController.js"></script>
  <script type="text/javascript" src="app/data.js"></script>
  <script type="text/javascript" src="app/services.js"></script>
  <script type="text/javascript" src="app/directives.js"></script>
  <script type="text/javascript" src="app/authentication.js"></script>
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67933427-1', 'auto');
  //ga('send', 'pageview');

</script>
</html>

