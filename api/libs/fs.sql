-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2015 at 10:31 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fs`
--

-- --------------------------------------------------------

--
-- Table structure for table `episode`
--

DROP TABLE IF EXISTS `episode`;
CREATE TABLE IF NOT EXISTS `episode` (
  `id` int(11) NOT NULL,
  `episode_num` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `points_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `episode`
--

INSERT INTO `episode` (`id`, `episode_num`, `season_id`, `points_text`) VALUES
(2, 1, 1, '<p><b>No Collar Wins Immunity:</b> Will, Vince, Joe, Nina, Hali, Jenn earn 2 points.<br /><b>Blue Collar Wins Immunity:</b> Rodney, Kelly, Dan, Mike, Lindsey, Sierra earn 2 points.</p>\n<p>So voted off.</p>'),
(3, 2, 1, '<p><b>White Collar Wins Immunity:</b> Carolyn, Joaquin, Max, Shirin, Tyler earn 2 points.<br>\n<b>Blue Collar Wins Immunity:</b> Rodney, Kelly, Dan, Mike, Lindsey, Sierra earn 2 points.</p>\n<p>Vince voted off.</p>'),
(4, 3, 1, '<p><b>Blue Collar Wins Immunity:</b> Rodney, Kelly, Dan, Mike, Lindsey, Sierra earn 2 points.<br>\n<b>White Collar Wins Immunity:</b> Carolyn, Joaquin, Max, Shirin, Tyler earn 2 points.</p>\n<p>Nina voted off.</p>'),
(8, 4, 1, '<p>Double Episode</p>\n<p><b>No Collar wins reward challenge:</b> Will, Joe, Hali, Jenn earn 1 point.<br>\n<b>White Collar wins reward challenge:</b> Carolyn, Joaquin, Max, Shirin, Tyler earn 1 point.</p>\n<p><b>Jenn finds hidden immunity idol:</b> earns 2 points.</p>\n<p><b>No Collar wins immunity:</b> Will, Joe, Hali, Jenn earn 2 points.<br>\n<b>White Collar wins immunity:</b> Carolyn, Joaquin, Max, Shirin, Tyler earn 2 points.</p>\n<p>Lindsey voted off.</p>\n<p><b>New Tribes:</b><br />\n<p><b>Escameca:</b> Sierra, Dan, Mike, Rodney, Tyler, Joaquin, Joe. <br />\n<b>Nagarote:</b> Will, Haley, Jenn, Max, Carolyn, Shirin, Kelly</p>\n<p><b>Escameca wins reward:</b> Sierra, Dan, Mike, Rodney, Tyler, Joaquin, Joe earn 1 point.<br />\n<b>Escameca wins immunity:</b> Sierra, Dan, Mike, Rodney, Tyler, Joaquin, Joe earn 2 points.</p>\n<p>Max voted off.</p>'),
(9, 5, 1, '<p><b>Nagarote wins reward challenge:</b> Will, Jenn, Carolyn, Kelly, Shirin, Hali earn 1 point.<br />\n<b>Nagarote wins immunity challenge:</b> Will, Jenn, Carolyn, Kelly, Shirin, Hali earn 2 points.</p>\n<p>Joaquin voted off.</p>'),
(10, 6, 1, '<p><b>Merge!</b> Will, Jenn, Carolyn, Kelly, Shirin, Hali, Dan, Joe, Sierra, Rodney, Tyler, Mike earn 1 point.<br />\n<b>Joe wins individual immunity:</b> 4 points</p>\n<p>Kelly voted off.</p>'),
(13, 7, 1, '<p><b>Joe wins individual reward challenge:</b> 2 points<br />\nJoe chooses Tyler, Will, Carolyn, and Shirin for reward: 1 point.<br />\n<b>Mike finds hidden immunity idol:</b> 2 points.<br />\n<b>Joe wins individual immunity challenge:</b> 4 points.</p>\n<p>Hali voted off.</p>'),
(14, 8, 1, '<p>Dan, Mike, Shirin, Tyler, Sierra win <b>reward challenge</b>: 1 point.<br />\n<b>Tyler wins individual immunity:</b> 4 points.</p>\n<p>Joe voted off.</p>'),
(15, 9, 1, '<p>Mike wins <b>individual immunity</b>: 4 points.</p>\n<p>Jenn voted off.</p>'),
(16, 10, 1, '<p>Tyler, Will, Dan, Carolyn win <b>reward challenge</b>: 1 point.<br />\nCarolyn and Mike win <b>individual immunity</b>: 4 points.</p>\n<p>Shirin voted off.</p>'),
(17, 11, 1, '<p>Mike, Sierra, and Carolyn win <b>reward challenge</b>: 1 point.<br />\nCarolyn wins <b>individual immunity</b>: 4 points.</p>\n<p>Tyler voted off.</p>'),
(18, 12, 1, '<p>Mike, Carolyn, Will win <b>reward challenge</b>: 1 point.<br />\nMike wins <b>individual immunity</b>: 4 points.</p>\n<p>Dan voted off.</p>'),
(19, 13, 1, '<p>MIke wins <b>reward challenge</b>: 2 points<br />\nMike wins <b>individual immunity</b>: 4 points.</p>\n<p>Sierra voted off.</p>\n<p>Mike wins <b>individual immunity</b>: 4 points.</p>\n<p>Rodney voted off.</p>\n<p><b>Mike is the sole survivor</b></p>');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
CREATE TABLE IF NOT EXISTS `points` (
  `id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `episode_id` int(11) NOT NULL,
  `survivor_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `reason` text CHARACTER SET utf8
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `season_id`, `episode_id`, `survivor_id`, `amount`, `reason`) VALUES
(13, 1, 2, 2, 2, 'Blue Collar Wins Immunity'),
(14, 1, 2, 5, 2, 'Blue Collar Wins Immunity'),
(15, 1, 2, 8, 2, 'Blue Collar Wins Immunity'),
(16, 1, 2, 11, 2, 'Blue Collar Wins Immunity'),
(17, 1, 2, 14, 2, 'Blue Collar Wins Immunity'),
(18, 1, 2, 17, 2, 'Blue Collar Wins Immunity'),
(19, 1, 2, 3, 2, 'No Collar Wins Immunity'),
(20, 1, 2, 6, 2, 'No Collar Wins Immunity'),
(21, 1, 2, 9, 2, 'No Collar Wins Immunity'),
(22, 1, 2, 12, 2, 'No Collar Wins Immunity'),
(23, 1, 2, 15, 2, 'No Collar Wins Immunity'),
(24, 1, 2, 18, 2, 'No Collar Wins Immunity'),
(25, 1, 2, 1, 0, ''),
(26, 1, 2, 4, 0, ''),
(27, 1, 2, 7, 0, ''),
(28, 1, 2, 10, 0, ''),
(29, 1, 2, 13, 0, ''),
(30, 1, 2, 16, 0, ''),
(31, 1, 3, 2, 2, 'Blue Collar Wins Immunity'),
(32, 1, 3, 5, 2, 'Blue Collar Wins Immunity'),
(33, 1, 3, 8, 2, 'Blue Collar Wins Immunity'),
(34, 1, 3, 11, 2, 'Blue Collar Wins Immunity'),
(35, 1, 3, 14, 2, 'Blue Collar Wins Immunity'),
(36, 1, 3, 17, 2, 'Blue Collar Wins Immunity'),
(37, 1, 3, 3, 0, ''),
(38, 1, 3, 6, 0, ''),
(39, 1, 3, 9, 0, ''),
(40, 1, 3, 12, 0, ''),
(41, 1, 3, 15, 0, ''),
(42, 1, 3, 18, 0, ''),
(43, 1, 3, 1, 2, 'White Collar Wins Immunity'),
(44, 1, 3, 4, 2, 'White Collar Wins Immunity'),
(45, 1, 3, 7, 2, 'White Collar Wins Immunity'),
(46, 1, 3, 13, 2, 'White Collar Wins Immunity'),
(47, 1, 3, 16, 2, 'White Collar Wins Immunity'),
(48, 1, 4, 2, 2, 'Blue Collar Wins Immunity'),
(49, 1, 4, 5, 2, 'Blue Collar Wins Immunity'),
(50, 1, 4, 8, 2, 'Blue Collar Wins Immunity'),
(51, 1, 4, 11, 2, 'Blue Collar Wins Immunity'),
(52, 1, 4, 14, 2, 'Blue Collar Wins Immunity'),
(53, 1, 4, 17, 2, 'Blue Collar Wins Immunity'),
(54, 1, 4, 3, 0, ''),
(55, 1, 4, 6, 0, ''),
(56, 1, 4, 9, 0, ''),
(57, 1, 4, 12, 0, ''),
(58, 1, 4, 18, 0, ''),
(59, 1, 4, 1, 2, 'White Collar Wins Immunity'),
(60, 1, 4, 4, 2, 'White Collar Wins Immunity'),
(61, 1, 4, 7, 2, 'White Collar Wins Immunity'),
(62, 1, 4, 13, 2, 'White Collar Wins Immunity'),
(63, 1, 4, 16, 2, 'White Collar Wins Immunity'),
(79, 1, 8, 2, 3, 'Escameca wins reward, Escameca wins immunity'),
(80, 1, 8, 5, 0, ''),
(81, 1, 8, 8, 0, ''),
(82, 1, 8, 11, 0, ''),
(83, 1, 8, 14, 3, 'Escameca wins reward, Escameca wins immunity'),
(84, 1, 8, 17, 3, 'Escameca wins reward, Escameca wins immunity'),
(85, 1, 8, 3, 3, 'No Collar wins reward challenge, No Collar wins immunity'),
(86, 1, 8, 6, 5, 'No Collar wins reward challenge, No Collar wins immunity, immunity idol found'),
(87, 1, 8, 9, 6, 'No Collar wins reward challenge, No Collar wins immunity, Escameca wins reward, Escameca wins immunity'),
(88, 1, 8, 18, 3, 'No Collar wins reward challenge, No Collar wins immunity'),
(89, 1, 8, 1, 3, 'White Collar wins reward challenge, White Collar wins immunity'),
(90, 1, 8, 4, 6, 'White Collar wins reward challenge, White Collar wins immunity, Escameca wins reward, Escameca wins immunity'),
(91, 1, 8, 7, 3, 'White Collar wins reward challenge, White Collar wins immunity'),
(92, 1, 8, 13, 3, 'White Collar wins reward challenge, White Collar wins immunity'),
(93, 1, 8, 16, 6, 'White Collar wins reward challenge, White Collar wins immunity, Escameca wins reward, Escameca wins immunity'),
(94, 1, 9, 2, 0, ''),
(95, 1, 9, 5, 3, 'Nagarote wins reward and immunity challenge.'),
(96, 1, 9, 11, 0, ''),
(97, 1, 9, 14, 0, ''),
(98, 1, 9, 17, 0, ''),
(99, 1, 9, 3, 3, 'Nagarote wins reward and immunity challenge.'),
(100, 1, 9, 6, 3, 'Nagarote wins reward and immunity challenge.'),
(101, 1, 9, 9, 0, ''),
(102, 1, 9, 18, 3, 'Nagarote wins reward and immunity challenge.'),
(103, 1, 9, 1, 3, 'Nagarote wins reward and immunity challenge.'),
(104, 1, 9, 4, 0, ''),
(105, 1, 9, 13, 3, 'Nagarote wins reward and immunity challenge.'),
(106, 1, 9, 16, 0, ''),
(107, 1, 10, 2, 1, 'Tribe merge.'),
(108, 1, 10, 5, 1, 'Tribe merge.'),
(109, 1, 10, 11, 1, 'Tribe merge.'),
(110, 1, 10, 14, 1, 'Tribe merge.'),
(111, 1, 10, 17, 1, 'Tribe merge.'),
(112, 1, 10, 3, 1, 'Tribe merge.'),
(113, 1, 10, 6, 1, 'Tribe merge.'),
(114, 1, 10, 9, 5, 'Tribe merge, Individual immunity.'),
(115, 1, 10, 18, 1, 'Tribe merge.'),
(116, 1, 10, 1, 1, 'Tribe merge.'),
(117, 1, 10, 13, 1, 'Tribe merge.'),
(118, 1, 10, 16, 1, 'Tribe merge.'),
(119, 1, 13, 2, 0, ''),
(120, 1, 13, 11, 2, 'Hidden immunity idol found.'),
(121, 1, 13, 14, 0, ''),
(122, 1, 13, 17, 0, ''),
(123, 1, 13, 3, 0, ''),
(124, 1, 13, 6, 0, ''),
(125, 1, 13, 9, 6, 'Wins individual reward challenge and individual immunity challenge.'),
(126, 1, 13, 18, 1, 'Chosen for reward.'),
(127, 1, 13, 1, 1, 'Chosen for reward.'),
(128, 1, 13, 13, 1, 'Chosen for reward.'),
(129, 1, 13, 16, 1, 'Chosen for reward.'),
(130, 1, 14, 2, 1, 'Reward challenge won.'),
(131, 1, 14, 11, 1, 'Reward challenge won.'),
(132, 1, 14, 14, 0, ''),
(133, 1, 14, 17, 1, 'Reward challenge won.'),
(134, 1, 14, 6, 0, ''),
(135, 1, 14, 9, 0, ''),
(136, 1, 14, 18, 0, ''),
(137, 1, 14, 1, 0, ''),
(138, 1, 14, 13, 1, 'Reward challenge won.'),
(139, 1, 14, 16, 5, 'Reward challenge won,  individual immunity.'),
(140, 1, 15, 2, 0, ''),
(141, 1, 15, 11, 4, 'Individual immunity won.'),
(142, 1, 15, 14, 0, ''),
(143, 1, 15, 17, 0, ''),
(144, 1, 15, 6, 0, ''),
(145, 1, 15, 18, 0, ''),
(146, 1, 15, 1, 0, ''),
(147, 1, 15, 13, 0, ''),
(148, 1, 15, 16, 0, ''),
(149, 1, 16, 2, 1, 'Reward challenge won.'),
(150, 1, 16, 11, 4, 'Individual immunity won.'),
(151, 1, 16, 14, 0, ''),
(152, 1, 16, 17, 0, ''),
(153, 1, 16, 18, 1, 'Reward challenge won.'),
(154, 1, 16, 1, 5, 'Reward challenge won. Individual immunity won.'),
(155, 1, 16, 13, 0, ''),
(156, 1, 16, 16, 1, 'Reward challenge won.'),
(157, 1, 17, 2, 0, ''),
(158, 1, 17, 11, 1, 'Reward challenge won.'),
(159, 1, 17, 14, 0, ''),
(160, 1, 17, 17, 1, 'Reward challenge won.'),
(161, 1, 17, 18, 0, ''),
(162, 1, 17, 1, 5, 'Reward challenge won. Individual immunity won.'),
(163, 1, 17, 16, 0, ''),
(164, 1, 18, 2, 0, ''),
(165, 1, 18, 11, 5, 'Reward challenge won. Individual immunity won.'),
(166, 1, 18, 14, 0, ''),
(167, 1, 18, 17, 0, ''),
(168, 1, 18, 18, 1, 'Reward challenge won.'),
(169, 1, 18, 1, 1, 'Reward challenge won.'),
(170, 1, 19, 11, 10, 'Reward challenge won. Individual immunity won.'),
(171, 1, 19, 14, 0, ''),
(172, 1, 19, 17, 0, ''),
(173, 1, 19, 18, 0, ''),
(174, 1, 19, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
CREATE TABLE IF NOT EXISTS `season` (
  `id` int(11) NOT NULL,
  `season_num` smallint(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `season`
--

INSERT INTO `season` (`id`, `season_num`, `name`, `location`) VALUES
(1, 30, 'Worlds Apart', 'San Juan del Sur, Nicaragua');

-- --------------------------------------------------------

--
-- Table structure for table `survivor`
--

DROP TABLE IF EXISTS `survivor`;
CREATE TABLE IF NOT EXISTS `survivor` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `age` smallint(6) DEFAULT NULL,
  `residence` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `voted_off` tinyint(1) NOT NULL,
  `tribe_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survivor`
--

INSERT INTO `survivor` (`id`, `name`, `photo`, `age`, `residence`, `occupation`, `voted_off`, `tribe_id`) VALUES
(1, 'Carolyn Rivera', 's30_carolyn.jpg', 52, 'Tampa, Fla.', 'Corporate Executive', 0, 4),
(2, 'Dan Foley', 's30_dan.jpg', 47, 'Gorham, Maine', 'Postal Worker', 1, 2),
(3, 'Hali Ford', 's30_hali.jpg', 25, 'San Francisco, California', 'Law Student', 1, 3),
(4, 'Joaquin Souberbielle', 's30_joaquin.jpg', 27, 'Valley Stream, New York', 'Marketing Director', 1, 4),
(5, 'Kelly Remington', 's30_kelly.jpg', 44, 'Grand Island, New York', 'New York State Police Investigator', 1, 2),
(6, 'Jenn Brown', 's30_jennifer.jpg', 22, 'Long Beach, California', 'Sailing Instructor', 1, 3),
(7, 'Max Dawson', 's30_max.jpg', 37, 'Topanga, California', 'Ph.D. - L.A. Based Media Consultant', 1, 4),
(8, 'Lindsey Cascaddan', 's30_lindsey.jpg', 24, 'College Park, Florida', 'Hairstylist', 1, 2),
(9, 'Joseph Anglim', 's30_joe.jpg', 25, 'Scottsdale, Arizona', 'Jewelry Designer', 1, 3),
(10, 'So Kim', 's30_so.jpg', 30, 'Long Beach, California', 'Retail Purchasing Agent', 1, 4),
(11, 'Mike Holloway', 's30_mike.jpg', 38, 'North Richland Hills, Texas', 'Oil Driller', 0, 2),
(12, 'Nina Poersch', 's30_nina.jpg', 51, 'Palmdale, California', 'Hearing Advocate', 1, 3),
(13, 'Shirin Oskooi', 's30_shirin.jpg', 31, 'San Francisco, California', 'Yahoo Executive', 1, 4),
(14, 'Rodney Lavoie, Jr.', 's30_rodney.jpg', 24, 'Boston, Massachusetts', 'General Contractor', 1, 2),
(15, 'Vince Sly', 's30_vince.jpg', 32, 'Santa Monica, California', 'Coconut Vendor', 1, 3),
(16, 'Tyler Fredrickson', 's30_tyler.jpg', 33, 'Los Angeles, California', 'Ex Talent Agent Assistant', 1, 4),
(17, 'Sierra Dawn Thomas', 's30_sierra.jpg', 27, 'Roy, Utah', 'Model/Professional Barrel Racer/Criminal Justice Student', 1, 2),
(18, 'Will Sims', 's30_will.jpg', 41, 'Sherman Oaks, California', 'YouTube Sensation', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tribe`
--

DROP TABLE IF EXISTS `tribe`;
CREATE TABLE IF NOT EXISTS `tribe` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `season_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tribe`
--

INSERT INTO `tribe` (`id`, `name`, `season_id`) VALUES
(1, 'Merica', 1),
(2, 'Blue Collar - Escameca', 1),
(3, 'No Collar - Nagarote', 1),
(4, 'White Collar - Masaya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `survivor_1_id` int(11) DEFAULT NULL,
  `survivor_2_id` int(11) DEFAULT NULL,
  `token` varchar(240) DEFAULT NULL,
  `token_expiration` datetime NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `admin`, `fname`, `lname`, `email`, `password`, `survivor_1_id`, `survivor_2_id`, `token`, `token_expiration`, `created`, `updated`) VALUES
(1, 1, 'Erik', 'Orwoll', 'eorwoll@gmail.com', '$2a$10$c81dc19cb14e81674773bugd0GvSERsJ5qkQ5FynOqF1uuMsmDSYq', 9, 12, '553a6c5685a607e2013675edf5a1278b', '2015-10-21 18:00:21', '2015-04-03 18:46:11', '2015-09-22 02:55:43'),
(8, 1, 'Ryan', 'Orwoll', 'ryanorwoll@gmail.com', '$2a$10$7d2cfab45a0145ae8cdeceJQiF8oVq5mrDEr7IARoGgGZQDjHJ5bq', 10, 16, NULL, '0000-00-00 00:00:00', '2015-04-29 21:54:11', '2015-04-30 13:14:29'),
(9, 0, 'Lori', 'Orwoll', 'loriorwoll@hotmail.com', '$2a$10$461afa6fdfcb18da43cd3uygI7WKHHUdeNqCicFH5bKy0lDnk.GH6', 2, 18, '4d10691ee243905c8208ec36fed2357b', '2015-10-16 20:06:12', '2015-08-26 16:44:46', '2015-09-16 18:06:12'),
(10, 0, 'Andy', 'Odegaard', 'aodegaard@isd2144.org', '$2a$10$3368a31d8926183bb9ebauhZDrSoWiyaCj7Ex80tGpxuOQUKHy2i2', 6, 15, NULL, '0000-00-00 00:00:00', '2015-08-26 16:46:20', '2015-08-26 14:46:20'),
(11, 0, 'Brianna', 'Odegaard', 'briodegaard3@gmail.com', '$2a$10$7eb17862c9949f81d8781uuciebwwmDSWqhFF.7JYheZbKqkqJ/Um', 1, 8, NULL, '0000-00-00 00:00:00', '2015-08-26 16:47:32', '2015-08-26 14:47:32'),
(12, 0, 'Kari', 'Odegaard', 'kodegaard@isd12.org', '$2a$10$f5b3baf8a19c494ec5e87uvcJLHZiYQxpnx73QJg1OWfMfsiNCIeC', 5, 4, NULL, '0000-00-00 00:00:00', '2015-08-26 16:48:12', '2015-08-26 14:48:12'),
(13, 0, 'Luke', 'Odegaard', 'laodegaard@gmail.com', '$2a$10$fb7b4089aa07fa845eff5uEDCLGzcBdt8oi1cLlL.MIsgi./W8VnK', 7, 17, NULL, '0000-00-00 00:00:00', '2015-08-26 16:48:53', '2015-08-26 14:48:53'),
(14, 0, 'Kara', 'Odegaard', 'kjorwoll@gmail.com', '$2a$10$6e2f54524592a13ed0551ugQ4./Vg.ej0rlTkfN1iqwtjYBtuRDdu', 13, 14, NULL, '0000-00-00 00:00:00', '2015-08-26 16:49:30', '2015-08-26 14:49:30'),
(15, 0, 'Ken', 'Orwoll', 'kenorwoll@gmail.com', '$2a$10$c0d7b57ca18cad20e1da6eXWQF5cD/KO8gbc2ORanslP3t4BZnIfG', 11, 3, NULL, '0000-00-00 00:00:00', '2015-08-26 16:50:11', '2015-08-26 14:50:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `episode`
--
ALTER TABLE `episode`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`), ADD KEY `survivor_id` (`survivor_id`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survivor`
--
ALTER TABLE `survivor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tribe`
--
ALTER TABLE `tribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD KEY `survivor_1_id` (`survivor_1_id`,`survivor_2_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `episode`
--
ALTER TABLE `episode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT for table `season`
--
ALTER TABLE `season`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `survivor`
--
ALTER TABLE `survivor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tribe`
--
ALTER TABLE `tribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
