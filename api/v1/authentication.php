<?php 

$app->get('/session', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    $response["uid"] = $session['uid'];
    $response["email"] = $session['email'];
    $response["name"] = $session['name'];
    echoResponse(200, $session);
});
$app->get('/logged-user', function () {
    $response = array();
    $headers = apache_request_headers();

    foreach ($headers as $header => $value) {
        if($header == "Authorization") {
            $token = $value;
            break;
        }
    }
    $currentDate = date('Y-m-d H:i:s');
    $db = new DbHandler();
    $user = $db->getOneRecord("select id,admin,fname,lname,email,token,token_expiration from user where token='$token'");
    
    if ($user != NULL) {
        if($currentDate < $user['token_expiration']) {
            //$response["grav_url"] = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( "ryanorwoll@gmail.com" ) ) ) . "?s=18" . "&d=" . urlencode( "http://www.fantasysurvivoronline.com/images/default-avatar.png" );
            $response['status'] = "success";
            $response['name'] = $user['fname']." ".$user['lname'];
            $response['uid'] = $user['id'];
            $response['email'] = $user['email'];
            $response['role'] = ($user['admin'] == 1 ? "admin" : "user");
        } else {
            $response['status'] = "error";
            $response['message'] = 'Token expired';
        }
    }else {
        $response['status'] = "error";
        $response['message'] = 'Please login.';
    }
    echoResponse(200, $response);
});
$app->post('/login', function() use ($app) {
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('email', 'password'),$r);
    $response = array();
    $db = new DbHandler();
    $password = $r->password;
    $email = $r->email;
    $user = $db->getOneRecord("select id,admin,fname,lname,password,email from user where email='$email'");
    if ($user != NULL) {
        if(passwordHash::check_password($user['password'],$password)){
            session_start();
            $_SESSION['uid'] = $user['id'];
            //$response['id'] = session_id();
            $response['status'] = "success";
            $response['message'] = 'Logged in successfully.';
            $response['name'] = $user['fname']." ".$user['lname'];
            $response['uid'] = $user['id'];
            $response['email'] = $user['email'];
            $response['role'] = ($user['admin'] == 1 ? "admin" : "user");
            $tokenExpiration = date('Y-m-d H:i:s', strtotime('+1 month'));
            $response['token'] = Tokenizer::createToken($user['id'], $tokenExpiration);
            $response['tokenExp'] = $tokenExpiration;
        } else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
    }else {
        $response['status'] = "error";
        $response['message'] = 'Login failed';
    }
    echoResponse(200, $response);
});
$app->post('/signUp', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    
    require_once 'passwordHash.php';
    $db = new DbHandler();
    $fname = $r->fname;
    $lname = $r->lname;
    $email = $r->email;
    $password = $r->password;
    $isUserExists = $db->getOneRecord("select 1 from user where email='$email'");
    if(!$isUserExists){
        $r->password = passwordHash::hash($password);
        $r->created = date('Y-m-d H:i:s');
        $table_name = "user";
        $column_names = array('fname', 'lname', 'email', 'password','created');
        $result = $db->insertIntoTable($r, $column_names, $table_name);
        
        if ($result !== NULL) {
            $response["status"] = "success";
            $response["message"] = "User account created successfully";
            $response["uid"] = $result;
            $response["fname"] = $fname;
            $response["lname"] = $lname;
            $response["email"] = $email;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create customer. Please try again";
            echoResponse(201, $response);
        }          
    }else{
        $response["status"] = "error";
        $response["message"] = "A user with the provided email already exists!";
        echoResponse(201, $response);
    }
});
$app->get('/logout', function() {
    $db = new DbHandler();
    //$session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Logged out successfully";
    echoResponse(200, $response);
});
?>