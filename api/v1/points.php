<?php 

$app->post('/episodes', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data->season_id;
    $r = $db->getAllRecords("SELECT * FROM episode WHERE season_id='$season_id' ORDER BY episode_num Desc") or die($query."<br>".$this->conn->error.__LINE__);
    echoResponse(200, $r);
});

$app->post('/episodes/latest', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data;
    $r = $db->getOneRecord("SELECT * FROM episode ORDER BY id DESC");
    echoResponse(200, $r);
});

$app->post('/episode/view', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data;
    $r = $db->getAllRecords("SELECT e.id, e.episode_num, e.points_text, p.id as pid, p.amount, p.reason, s.name AS 'survivor' FROM episode e INNER JOIN points p ON p.episode_id = e.id INNER JOIN survivor s ON p.survivor_id = s.id WHERE p.episode_id = '$id'");
    
    echoResponse(200, $r);
});

$app->post('/episode/edit', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data;
    $r = $db->getOneRecord("SELECT id, episode_num, points_text FROM episode WHERE id = '$id'");
    
    echoResponse(200, $r);
});

$app->post('/episode/add', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    
    $db = new DbHandler();
    //$episodeNum = $r->data->episode->num;
    //$text = $r->data->episode->text;
    $season_id = $r->data->episode->season_id;
    
    $table_name = "episode";
    $column_names = array('episode_num', 'season_id', 'points_text');
    $episode_id = $db->insertIntoTable($r->data->episode, $column_names, $table_name);
      
    if ($episode_id !== NULL) {
        $query = "INSERT INTO points (season_id, episode_id, survivor_id, amount) VALUES ";
        foreach($r->data->survivors as $survivor) {
            if(array_key_exists("amount", $survivor)) {
                $query .= "(".$season_id.", ".$episode_id.", ".$survivor->id.", ".$survivor->amount."),";
            } else {
                $query .= "(".$season_id.", ".$episode_id.", ".$survivor->id.", 0),";
            }
        }
        $query = rtrim($query, ",");
        $result = $db->updateRecord($query) or die($query."<br>".$this->conn->error.__LINE__);

        if($result) {
            $response["status"] = "success";
            $response["message"] = "Episode added successfully";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to add episode. Please try again";
            echoResponse(201, $response);
        }
    }
});

$app->put('/episode/update', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data->id;
    $text = $r->data->points_text;
    $episode_num = $r->data->episode_num;

    $result = $db->updateRecord("UPDATE episode SET episode_num='$episode_num', points_text='$text' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Episode updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update episode. Please try again";
        echoResponse(201, $response);
    }
});

$app->put('/points/update', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data->id;
    $reason = $r->data->reason;
    $points = $r->data->points;

    $result = $db->updateRecord("UPDATE points SET reason='$reason', amount='$points' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Points updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update points. Please try again";
        echoResponse(201, $response);
    }
});

$app->delete('/episode/delete', function () use ($app) {
    $response = array();
    $id = json_decode($app->request->getBody());
    $db = new DbHandler();

    $result = $db->updateRecord("DELETE FROM episode WHERE id='$id'");
    if($result) {
        $points_result = $db->updateRecord("DELETE FROM points WHERE episode_id='$id'");
        if($points_results) {
            $response["status"] = "success";
            $response["message"] = "Episode deleted successfully";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to delete points. Please try again";
            echoResponse(201, $response);
        }
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to delete episode. Please try again";
        echoResponse(201, $response);
    }
});

$app->post('/survivor/points', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data;
    $r = $db->getOneRecord("SELECT p.id, p.episode_id, p.amount, p.reason, s.name FROM points p INNER JOIN survivor s ON p.survivor_id = s.id WHERE p.id = '$id'");
    
    echoResponse(200, $r);
})
?>