<?php 

$app->get('/survivors', function () {
	$response = array();
	$db = new DbHandler();
    $r = $db->getAllRecords("SELECT survivor.*, tribe.name as 'tribe'
                                FROM survivor 
                                JOIN tribe ON tribe.id = survivor.tribe_id
                                JOIN season ON tribe.season_id = season.id && season.season_num = 30");
    
    echoResponse(200, $r);
});

$app->post('/currentSurvivors', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data->season_id;
    $result = $db->getAllRecords("SELECT survivor.*, tribe.name as 'tribe', season.season_num as 'season'
                                FROM survivor 
                                JOIN tribe ON tribe.id = survivor.tribe_id
                                JOIN season ON tribe.season_id = season.id WHERE season.id = '$season_id'");
    if($result) {
        echoResponse(200, $result);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to retrieve survivors. Please try again";
        echoResponse(201, $response);
    }
});

$app->post('/activeSurvivors', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data->season_id;
    $result = $db->getAllRecords("SELECT survivor.*, tribe.name as 'tribe', season.season_num as 'season'
                                FROM survivor 
                                JOIN tribe ON tribe.id = survivor.tribe_id
                                JOIN season ON tribe.season_id = season.id WHERE season.id = '$season_id' && survivor.voted_off = '0'");
    if($result) {
        echoResponse(200, $result);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to retrieve survivors. Please try again";
        echoResponse(201, $response);
    }
});

$app->post('/survivors/status', function() use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $id = $r->data->id;
    $voted_off = $r->data->voted_off;

    $result = $db->updateRecord("UPDATE survivor SET voted_off='$voted_off' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Survivor updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update survivor. Please try again";
        echoResponse(201, $response);
    }
});

$app->post('/survivor/get', function() use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $id = $r->data;

    $result = $db->getOneRecord("SELECT * from survivor WHERE id='$id'");
    if($result) {
        echoResponse(200, $result);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update survivor. Please try again";
        echoResponse(500, $response);
    }
});

$app->post('/survivors/add', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $name = $r->data->name;

    $isExists = $db->getOneRecord("SELECT 1 FROM survivor WHERE name='$name'");
    if(!$isExists){
        $table_name = "survivor";
        $column_names = array('name', 'photo', 'age', 'residence', 'occupation', 'voted_off', 'tribe_id');
        $result = $db->insertIntoTable($r->data, $column_names, $table_name);
        
        if ($result !== NULL) {
            $response["status"] = "success";
            $response["message"] = "Survivor created successfully";
            $response["id"] = $result;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create survivor. Please try again";
            echoResponse(201, $response);
        }          
    }
    else
    {
        $response["status"] = "error";
        $response["message"] = "A survivor with that name already exists";
        echoResponse(201, $response);
    }
});
$app->put('/survivors/edit', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data->id;
    $name = $r->data->name;
    $photo = $r->data->photo;
    $age = $r->data->age;
    $residence = $r->data->residence;
    $occupation = $r->data->occupation;
    $voted_off = $r->data->voted_off;
    $tribe_id = $r->data->tribe_id;

    $result = $db->updateRecord("UPDATE survivor SET name='$name', photo='$photo', age='$age', residence='$residence', occupation='$occupation', voted_off='$voted_off', tribe_id='$tribe_id' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Survivor updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update survivor. Please try again";
        echoResponse(500, $response);
    }
});
$app->delete('/survivors/delete', function () use ($app) {
    $response = array();
    $id = json_decode($app->request->getBody());
    $db = new DbHandler();

    $result = $db->updateRecord("DELETE FROM survivor WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Survivor deleted successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to delete survivor. Please try again";
        echoResponse(201, $response);
    }
})
?>