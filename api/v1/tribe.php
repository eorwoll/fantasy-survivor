<?php 

$app->get('/tribes', function () {
	$response = array();
	$db = new DbHandler();
    $r = $db->getAllRecords("select tribe.id, tribe.name, season.season_num from tribe left join season on season.id = tribe.season_id");
    
    echoResponse(200, $r);
});

$app->post('/currentTribes', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data->seasonId;
    $r = $db->getAllRecords("select tribe.id, tribe.name from tribe left join season on season.id = tribe.season_id WHERE season.id='$season_id'");
    
    echoResponse(200, $r);
});

$app->post('/tribes/add', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $name = $r->data->name;
    $season_id = $r->data->season_id;
    
    $isExists = $db->getOneRecord("SELECT 1 FROM tribe WHERE name='$name'");
    if(!$isExists){
        $table_name = "tribe";
        $column_names = array('name', 'season_id');
        $result = $db->insertIntoTable($r->data, $column_names, $table_name);
        
        if ($result !== NULL) {
            $response["status"] = "success";
            $response["message"] = "Tribe created successfully";
            $response["id"] = $result;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create tribe. Please try again";
            echoResponse(201, $response);
        }          
    }
    else
    {
        $response["status"] = "error";
        $response["message"] = "A tribe with that name already exists";
        echoResponse(201, $response);
    }
});
$app->put('/tribes/edit', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data->id;
    $name = $r->data->name;
    $season_id = $r->data->season_id;

    $result = $db->updateRecord("UPDATE tribe SET name='$name', season_id='$season_id' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Tribe updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update tribe. Please try again";
        echoResponse(201, $response);
    }
});
$app->delete('/tribes/delete', function () use ($app) {
    $response = array();
    $id = json_decode($app->request->getBody());
    $db = new DbHandler();

    $result = $db->updateRecord("DELETE FROM tribe WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Tribe deleted successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to delete tribe. Please try again";
        echoResponse(201, $response);
    }
})
?>