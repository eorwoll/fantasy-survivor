<?php 

$app->get('/allUsers', function () {
	$response = array();
    $db = new DbHandler();
    $r = $db->getAllRecords("SELECT * FROM user");
    
    echoResponse(200, $r);
});

$app->post('/users', function () use ($app){
	$response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $season_id = $r->data->season_id;
    $r = $db->getAllRecords("SELECT u.id, u.admin, u.fname, u.lname, u.email, us.survivor1_id, us.survivor2_id, s1.name AS 'survivor1', s2.name AS 'survivor2'
                            FROM user u 
                            INNER JOIN users_survivors us ON us.user_id = u.id
                            INNER JOIN survivor s1 ON us.survivor1_id = s1.id
                            INNER JOIN survivor s2 ON us.survivor2_id = s2.id WHERE us.season_id = '$season_id'");
    
    echoResponse(200, $r);
});

$app->post('/user/dashboard', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $id = $r->data->uid;
    $season_id = $r->data->season_id;
    $response = $db->getAllRecords("SELECT u.fname, u.lname, s.name, s.photo, s.voted_off 
                                    FROM user u 
                                    INNER JOIN users_survivors us ON us.user_id = u.id
                                    INNER JOIN survivor s ON us.survivor1_id = s.id || us.survivor2_id = s.id 
                                    WHERE u.id = '$id' && us.season_id = '$season_id'");
    if($response != NULL) {
        $response["status"] = "success";
        $standings = getStandings($season_id);
        $response["standings"] = $standings;
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to retrieve dashboard.";
        echoResponse(201, $response);
    }      
});

$app->post('/user/standings', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    $response["standings"] = getStandings($r->data->season_id);
    if($response["standings"] == "") {
        $response["status"] = "error";
        $response["message"] = "There was an error retreiving data";
        echoResponse(500, $response);
    } else {
        $response["status"] = "success";
        echoResponse(200, $response);
    }
});

$app->post('/user/profile', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();

    $id = $r->data;

    $response = $db->getOneRecord("SELECT fname, lname, email from user WHERE id='$id'");
    if($response != NULL) {
        $response["status"] = "success";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to retrieve profile.";
        echoResponse(201, $response);
    }
});

$app->post('/user/add', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    
    require_once 'passwordHash.php';
    $db = new DbHandler();
    
    $fname = $r->data->fname;
    $lname = $r->data->lname;
    $email = $r->data->email;

    $isUserExists = $db->getOneRecord("SELECT 1 FROM user WHERE email='$email'");
    if(!$isUserExists){
        $r->data->admin = 0;
        $r->data->created = date('Y-m-d H:i:s');
        $table_name = "user";
        $column_names = array('admin', 'fname', 'lname', 'email','created');
        $result = $db->insertIntoTable($r->data, $column_names, $table_name);
        
        if ($result !== NULL) {
            $response["status"] = "success";
            $response["message"] = "User account created successfully";
            $response["id"] = $result;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create user. Please try again";
            echoResponse(201, $response);
        }          
    }else{
        $response["status"] = "error";
        $response["message"] = "A user with the provided email already exists!";
        echoResponse(201, $response);
    }
});
$app->put('/user/edit', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();
    $id = $r->data->id;
    $admin = $r->data->admin;
    $fname = $r->data->fname;
    $lname = $r->data->lname;
    $email = $r->data->email;
    $survivor_1_id = $r->data->survivor_1_id;
    $survivor_2_id = $r->data->survivor_2_id;
    $updated = date('Y-m-d H:i:s');
    
    // Update password if changed
    if($r->data->password != null && $r->data->password != "")
    {
        // update password
        $password = passwordHash::hash($r->data->password);
        $passResult = $db->updateRecord("UPDATE user SET password='$password' WHERE id='$id'");
        if($passResult)
        {
            $response["passStatus"] = "password success";
        } else {
            $response["passStatus"] = "password error";
        }
    }

    $result = $db->updateRecord("UPDATE user SET admin='$admin', fname='$fname', lname='$lname', email='$email', survivor_1_id='$survivor_1_id', survivor_2_id='$survivor_2_id', updated='$updated' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "User updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update user. Please try again";
        echoResponse(201, $response);
    }
});

$app->put('/user/editProfile', function () use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $db = new DbHandler();

    $id = $r->data->id;
    $fname = $r->data->fname;
    $lname = $r->data->lname;
    $email = $r->data->email;
    $updated = date('Y-m-d H:i:s');
    
    // Update password if changed
    if($r->data->password != null && $r->data->password != "")
    {
        // update password
        $password = passwordHash::hash($r->data->password);
        $passResult = $db->updateRecord("UPDATE user SET password='$password' WHERE id='$id'");
        if($passResult)
        {
            $response["passStatus"] = "password success";
        } else {
            $response["passStatus"] = "password error";
        }
    }

    $result = $db->updateRecord("UPDATE user SET fname='$fname', lname='$lname', email='$email', updated='$updated' WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "Updated successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to update. Please try again";
        echoResponse(201, $response);
    }
});

$app->delete('/user/delete', function () use ($app) {
    $response = array();
    $id = json_decode($app->request->getBody());
    $db = new DbHandler();

    $result = $db->updateRecord("DELETE FROM user WHERE id='$id'");
    if($result) {
        $response["status"] = "success";
        $response["message"] = "User deleted successfully";
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to delete user. Please try again";
        echoResponse(201, $response);
    }
})
?>