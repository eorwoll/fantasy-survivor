<?php 

$app->get('/latest-season', function () {
    $response = array();
    $db = new DbHandler();
    
    $result = $db->getOneRecord('SELECT id FROM season ORDER BY season_num DESC');

    if ($result !== NULL) {
        $response["status"] = "success";
        $response["message"] = "Season retrieved successfully";
        $response["id"] = $result;
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to get latest season. Please try again";
        echoResponse(201, $response);
    }
});

$app->post('/newSeason', function () use ($app) {
    $response = array();
    $db = new DbHandler();
    $r = json_decode($app->request->getBody());
    
    $table_name = "season";
    $column_names = array('season_num', 'name', 'location');
    $result = $db->insertIntoTable($r->data, $column_names, $table_name);

    if ($result !== NULL) {
        $response["status"] = "success";
        $response["message"] = "Season created successfully";
        $response["id"] = $result;
        echoResponse(200, $response);
    } else {
        $response["status"] = "error";
        $response["message"] = "Failed to create season. Please try again";
        echoResponse(201, $response);
    }
});

?>