<?php

require_once 'dbHandler.php';
require_once 'passwordHash.php';
require_once 'Tokenizer.php';
require '.././libs/Slim/Slim.php';

Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

require_once 'authentication.php';
require_once 'user.php';
require_once 'tribe.php';
require_once 'survivor.php';
require_once 'points.php';
require_once 'season.php';

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields,$request_params) {
    $error = false;
    $error_fields = "";
    foreach ($required_fields as $field) {
        if (!isset($request_params->$field) || strlen(trim($request_params->$field)) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["status"] = "error";
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(200, $response);
        $app->stop();
    }
}

function getStandings($season_id) {
    $db = new DbHandler();
    $survivorPoints = $db->getAllRecords("SELECT s.name, s.voted_off, SUM(p.amount) as points from points p INNER JOIN survivor s ON s.id = p.survivor_id where p.season_id = $season_id GROUP BY p.survivor_id order by points");

    $users = $db->getAllRecords("SELECT u.id, u.fname, u.lname, us.survivor1_id, us.survivor2_id, s1.name AS 'survivor1', s2.name AS 'survivor2' 
                                FROM user u 
                                INNER JOIN users_survivors us ON us.user_id = u.id
                                INNER JOIN survivor s1 ON s1.id = us.survivor1_id 
                                INNER JOIN survivor s2 ON s2.id = us.survivor2_id WHERE us.season_id = '$season_id'");

    $stanings = [];
    for($x = 0; count($users) > $x; $x++ ) {
        $points = 0;
        
        for($y = 0; count($survivorPoints) > $y; $y++) {
            if($users[$x]["survivor1"] == $survivorPoints[$y]["name"] || $users[$x]["survivor2"] == $survivorPoints[$y]["name"]) {
                $points += $survivorPoints[$y]["points"];
            }
            if($users[$x]["survivor1"] == $survivorPoints[$y]["name"])
                $votedOff1 = (int)$survivorPoints[$y]['voted_off'];
            else if($users[$x]["survivor2"] == $survivorPoints[$y]["name"])
                $votedOff2 = (int)$survivorPoints[$y]['voted_off'];
        }

        $standings[$users[$x]["id"]]["points"] = $points;
        $standings[$users[$x]["id"]]["uid"] = $users[$x]["id"];
        $standings[$users[$x]["id"]]["user"] = $users[$x]["fname"];
        $standings[$users[$x]["id"]]["survivor1"] = $users[$x]["survivor1"];
        $standings[$users[$x]["id"]]["survivor2"] = $users[$x]["survivor2"];
        $standings[$users[$x]["id"]]['votedOff1'] = $votedOff1;
        $standings[$users[$x]["id"]]['votedOff2'] = $votedOff2;
    }
    return $standings;
}

function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>