import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'react-testing-library';

import axiosMock from 'axios';
import standingsData from '../__mocks__/dataMocks/standings';
import Standings from '../js/Standings';

afterEach(cleanup);

test('renders the standings', () => {
  const data = { season_id: 8 };

  axiosMock.post.mockImplementationOnce(() =>
    Promise.resolve({
      data: standingsData,
    })
  );
  const { container } = render(<Standings season_id={8} />);

  expect(axiosMock.post).toHaveBeenCalledTimes(1);
  expect(axiosMock.post).toHaveBeenCalledWith('/api/v1/user/standings', {
    data: data,
  });

  expect(container.firstChild).toMatchSnapshot();
});
