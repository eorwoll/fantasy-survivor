import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'react-testing-library';

import axiosMock from 'axios';
import episodeData from '../__mocks__/dataMocks/episodes';
import Episodes from '../js/Episodes';

afterEach(cleanup);

test('renders the episodes', () => {
  const data = { season_id: 8 };

  axiosMock.post.mockImplementationOnce(() =>
    Promise.resolve({
      data: episodeData,
    })
  );
  const { container } = render(<Episodes season_id={8} />);

  expect(axiosMock.post).toHaveBeenCalledTimes(1);
  expect(axiosMock.post).toHaveBeenCalledWith('/api/v1/episodes', {
    data: data,
  });

  expect(container.firstChild).toMatchSnapshot();
});
