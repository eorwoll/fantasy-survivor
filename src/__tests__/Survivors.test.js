import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'react-testing-library';

import axiosMock from 'axios';
import survivorsData from '../__mocks__/dataMocks/survivors';
import Survivors from '../js/Survivors';

afterEach(cleanup);

test('renders the episodes', () => {
  const data = { season_id: 8 };

  axiosMock.post.mockImplementationOnce(() =>
    Promise.resolve({
      data: survivorsData,
    })
  );
  const { container } = render(<Survivors season_id={8} />);

  expect(axiosMock.post).toHaveBeenCalledTimes(1);
  expect(axiosMock.post).toHaveBeenCalledWith('/api/v1/currentSurvivors', {
    data: data,
  });

  expect(container.firstChild).toMatchSnapshot();
});
