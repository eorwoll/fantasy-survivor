import React from 'react';

const Rules = () => (
  <div className="panel-wrapper">
    <h1>Rules</h1>
    <h2>Draft</h2>
    <ul>
      <li>
        The TV will be paused immediately prior to the first immunity challenge
        (No cell phones allowed after this point).
      </li>
      <li>
        Each player will draw a number out of a hat. That number will be your
        draft order.
      </li>
      <li>
        Player 1 makes the first pick, selecting any of the Survivor
        contestants.
      </li>
      <li>
        Player 2 makes the second pick, selecting any of the Survivor
        contestants not already picked. (Once a contestant is selected, he/she
        can no longer be drafted by other players)
      </li>
      <li>Players 3-9 continue making their picks.</li>
      <li>
        After all players have drafted one Survivor contestant, the draft order
        reverses. The player with the last pick in round one goes first in round
        two. (Snake draft style).
      </li>
      <li>
        The draft ends when everyone has 2 Survivor contestants. These two
        people make up your Fantasy Survivor Team.
      </li>
    </ul>

    <h2>Game #1: Sole Survivor</h2>
    <ul>
      <li>The player who has the contestant that wins Survivor wins.</li>
      <li>Entry Fee: $5</li>
      <li>Winner takes the prize money.</li>
    </ul>

    <h2>Game #2: Survivor Points</h2>
    <ul>
      <li>
        Each Player&apos;s Survivor team members accumulate points for acheiving
        different Survivor milestones.
      </li>
    </ul>

    <h2>Points are awarded as follows:</h2>
    <ul>
      <li>
        Tribe/team Reward <b>(1)</b>: Each Survivor who wins a tribe reward
        challenge receives 1 point. If a player has 2 survivors on the winning
        tribe they receive 1 point for each Survivor.
      </li>
      <li>
        Tribe Immunity <b>(2)</b>: Each Survivor who wins a tribe immunity
        challenge receives 2 points. If a player has 2 survivors on the winning
        tribe they receive 2 points for each Survivor.
      </li>
      <li>
        Merge <b>(1)</b>: A Survivor that is part of the merge to 1 tribe
        receives 1 point.
      </li>
      <li>
        Individual Reward - Win <b>(2)</b>: A Survivor who wins an individual
        reward challenge receives 2 points.
      </li>
      <li>
        Individual Reward - Chosen <b>(1)</b>: If the winning Survivor brings 1
        or more other Survivors on the reward, those Survivors receive 1 point.
      </li>
      <li>
        Give Away Individual Reward <b>(-1)</b>: A Survivor who gives away an
        individual reward loses 1 point.
      </li>
      <li>
        Gifted Individual Reward <b>(1)</b>: A Survivor who is given an
        individual reward won by another Survivor receives 1 point.
      </li>
      <li>
        Individual Immunity <b>(4)</b>: A Survivor who wins an individual
        immunity challenge receives 4 points.
      </li>
      <li>
        Medical Evacuation <b>(-2)</b>: A Survivor that is medically evacuated
        for any reason loses 2 points.
      </li>
      <li>
        Survivor Quits <b>(-2)</b>: If a Survivor quits for any reason the
        player loses 2 points.
      </li>
      <li>
        Exile Island Challenge Win <b>(1)</b>: A Survivor that wins an Exile
        Island Challenge receives 1 point.
      </li>
      <li>
        Exile Island <b>(-1)</b>: A Survivor that goes to Exile Island loses 1
        point. (Either by losing an Exile Island challenge or by being chosen to
        go)
      </li>
      <li>
        Find Hidden Idol <b>(2)</b>: A Survivor who finds a hidden immunity idol
        earns 2 points.
      </li>
      <li>
        Successfully use Hidden Idol <b>(2)</b>: A Survivor who uses a hidden
        immunity idol to avoid being voted off successfully earns 2 points.
      </li>
      <li>
        Give Away Hidden Immunity Idol <b>(-1)</b>: A Survivor who gives away a
        hidden immunity idol loses 1 point. (Must have been a hidden idol)
      </li>
      <li>
        Gifted Hidden Immunity Idol <b>(+1)</b>: A Survivor who is given a
        hidden immunity idol from another player receives 1 point. (Must have
        been a hidden idol).
      </li>
      <li>
        J&apos;Tia Rice Dump Penalty <b>(-3)</b>: A Survivor who intentionally
        dumps the rice loses 3 points.
      </li>
      <li>
        Russel Hantz Fire Penalty <b>(-3)</b>: A Survivor who intentionally
        throws another Survivor&apos;s belonging in the fire loses 3 points.
      </li>
      <li>
        Earn or gifted a fire token. <b>(1)</b> per token.
      </li>
    </ul>

    <p>
      Points are awarded only for events that occur after the draft has been
      completed. (Points will not be awarded retro-actively for events that
      occurred prior to us knowing who is on our fantasy team).
    </p>

    <p>
      Entry Fee: $5
      <br />
      The player with the most accumulated points (combining points from both
      Survivor Contestants on their fantasy team) wins.
      <br />
      Winner takes the prize money.
    </p>
  </div>
);

export default Rules;
