import { css } from '@emotion/core';
import { darkShade } from '../../css/jsVariables.scss';
import forSize from '../styles.breakpoints';

const grid = css`
  display: grid;
  display: -ms-grid;
  grid-template-columns: 150px 1fr 1fr;
  grid-template-rows: 50px;
  grid-column-gap: 10px;
  align-items: center;
  align-self: center;
  border-bottom: 1px solid ${darkShade};
  font-size: 0.8em;
`;

const styles = {
  standingsContainer: css`
    display: flex;
    flex-direction: column;
  `,

  standingsHeaders: css`
    ${grid}
    font-weight: bold;

    ${forSize('tablet-portrait-up')} {
      font-size: 1em;
      grid-template-columns: 30% 110px 1fr 100px;
      width: 75%;
    }

    ${forSize('tablet-portrait-dn')} {
      width: 90%;
    }

    ${forSize('desktop-up')} {
      width: 100%;
    }
  `,

  standingsItem: css`
    ${grid}

    ${forSize('tablet-portrait-up')} {
      font-size: 1em;
      grid-template-columns: 30% 110px 1fr 100px;
      width: 75%;
    }

    ${forSize('tablet-portrait-dn')} {
      width: 90%;
    }

    ${forSize('desktop-up')} {
      width: 100%;
    }
  `,

  headerSurvivors: css`
    ${forSize('phone-only')} {
      display: none;
    }
  `,

  itemSurvivors: css`
    ${forSize('phone-only')} {
      display: none;
    }
  `,

  headerPoints: css`
    ${forSize('phone-only')} {
      text-align: center;
    }
  `,

  headerRemaining: css`
    ${forSize('phone-only')} {
      text-align: center;
    }
  `,

  itemPoints: css`
    ${forSize('phone-only')} {
      text-align: center;
    }
  `,

  itemSurvivorSame: css`
    ${forSize('tablet-portrait-dn')} {
      display: block;
    }
  `,
};

export default styles;
