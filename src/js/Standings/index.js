import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import styles from './styles';

function getRemaining(s1, s2) {
  if (s1 === 1 && s2 === 1) return 0;
  else if (s1 === 1 || s2 === 1) return 1;
  else return 2;
}

/**
 * Renders a table of standings for current season
 */
function Standings({ season_id }) {
  const data = { season_id: season_id };

  const [isLoading, setIsLoading] = useState(true);
  const [standings, setStandings] = useState([]);

  useEffect(() => {
    axios
      .post('/api/v1/user/standings', { data: data })
      .then(response => {
        const { standings } = response.data;
        let sortedStandings = [];
        Object.keys(standings).forEach(key => {
          sortedStandings.push([key, standings[key]]);
          sortedStandings.sort((a, b) => {
            return b[1].points - a[1].points;
          });
        });
        setIsLoading(false);
        setStandings(sortedStandings);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  return (
    <div className={'panel-wrapper ' + (isLoading ? 'hidden' : '')}>
      <div css={styles.standingsContainer}>
        <h1>Standings</h1>
        <div css={styles.standingsHeaders}>
          <div />
          <div>Remaining</div>
          <div css={styles.headerSurvivors}>Survivors</div>
          <div css={styles.headerPoints}>Points</div>
        </div>
        {standings.map((user, index) => {
          return (
            <div css={styles.standingsItem} key={user[1].uid}>
              <div>
                <strong>{index + 1}.</strong>
                &nbsp;&nbsp;
                {user[1].user}
              </div>
              <div>{getRemaining(user[1].votedOff1, user[1].votedOff2)}</div>
              <div css={styles.itemSurvivors}>
                <span
                  css={styles.itemSurvivorName}
                  className={`${user[1].votedOff1 ? 'strike' : ''}`}
                >
                  {user[1].survivor1},{' '}
                </span>
                <span
                  css={styles.itemSurvivorName}
                  className={`${user[1].votedOff2 ? 'strike' : ''}`}
                >
                  {user[1].survivor2}
                </span>
              </div>
              <div css={styles.itemPoints}>
                <strong>{user[1].points}</strong>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

Standings.propTypes = {
  /** Current season ID */
  season_id: PropTypes.number.isRequired,
};
export default Standings;
