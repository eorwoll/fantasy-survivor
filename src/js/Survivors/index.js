import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import styles from './styles';

class Survivors extends React.Component {
  state = {
    isLoading: true,
    survivors: [],
  };

  componentDidMount() {
    let { season_id } = this.props;
    let data = { season_id: season_id };

    axios
      .post('/api/v1/currentSurvivors', { data: data })
      .then(response => {
        this.setState({
          isLoading: false,
          survivors: response.data,
        });
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  render() {
    let { isLoading, survivors } = this.state;

    return (
      <div className={'panel-wrapper ' + (isLoading ? 'hidden' : '')}>
        <h1>Season 40 Cast</h1>
        <div css={styles.survivorsContainer}>
          {survivors.map(s => {
            return (
              <div css={styles.survivor} key={s.id}>
                <div>
                  <img src={`images/survivors/s40/${s.photo}`} alt={s.name} />
                </div>
                <div css={styles.survivorInfo}>
                  <h4>{s.name}</h4>
                  {s.voted_off === '1' && (
                    <div css={styles.votedOff}>
                      <span>VOTED OFF</span>
                    </div>
                  )}
                  <div>
                    <strong>Age:</strong> {s.age}
                  </div>
                  <div>
                    <strong>Residence:</strong> {s.residence}
                  </div>
                  <div className={s.occupation !== '' ? '' : 'hide'}>
                    <strong>Occupation:</strong> {s.occupation}
                  </div>
                  <div>
                    <strong>Tribe:</strong> {s.tribe}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

Survivors.propTypes = {
  /** Current season ID */
  season_id: PropTypes.number.isRequired,
};
export default Survivors;
