import { css } from '@emotion/core';
import { headerFont, white } from '../../css/jsVariables.scss';
import forSize from '../styles.breakpoints';

const styles = {
  survivorsContainer: css`
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
  `,

  survivor: css`
    max-width: 270px;
    margin-bottom: 40px;

    ${forSize('phone-only')} {
      width: 320px;
    }

    img {
      width: 280px;
      margin-bottom: 14px;
    }
  `,

  survivorInfo: css`
    width: 300px;
    h4 {
      font-family: ${headerFont};
    }
  `,

  votedOff: css`
    margin-bottom: 20px;

    span {
      background-color: red;
      border-radius: 4px;
      padding: 3px 9px;
      color: ${white};
      font-weight: 500;
    }
  `,
};

export default styles;
