import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const ToastContainer = styled.div`
  position: absolute;
  width: 100%;
  background-color: limegreen;
  color: white;
  padding: 14;
  left: 0;
  text-align: center;
  display: ${props => (props.isShowing ? '' : 'none')};
`;

class Toast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowing: false,
      message: '',
    };

    this.show = this.show.bind(this);
  }

  show(message) {
    const toast = { ...this.state };
    toast.message = message;
    toast.isShowing = true;
    this.setState(toast);
  }

  render() {
    const { isShowing, message } = this.state;

    return (
      <ToastContainer isShowing={isShowing}>
        <div>{message}</div>
      </ToastContainer>
    );
  }
}

export default Toast;
