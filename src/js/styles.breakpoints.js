export default function forSize(size) {
  if (size === 'phone-only') {
    return '@media (max-width: 599px)';
  } else if (size === 'tablet-portrait-dn') {
    return '@media (max-width: 768px)';
  } else if (size === 'tablet-portrait-up') {
    return '@media (min-width: 768px)';
  } else if (size === 'tablet-landscape-up') {
    return '@media (min-width: 900px)';
  } else if (size === 'desktop-up') {
    return '@media (min-width: 1024px)';
  }
}
