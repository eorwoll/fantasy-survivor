import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

const { shape, arrayOf, string, object } = PropTypes;

class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  toggle() {
    let { isOpen } = this.state;

    this.setState({
      isOpen: !isOpen,
    });
  }

  render() {
    let { isOpen } = this.state;
    const { nav } = this.props;
    const { title, items, backgroundColor } = nav;

    const navItems = items.map(item => {
      return (
        <NavItem key={item.name}>
          <NavLink tag={Link} to={item.url}>
            {item.name}
          </NavLink>
        </NavItem>
      );
    });

    return (
      <div>
        <Navbar light expand="md" style={{ backgroundColor: backgroundColor }}>
          <NavbarBrand href="/">{title}</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {navItems}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

Navigation.propTypes = {
  nav: shape({
    title: string.isRequired,
    items: arrayOf(object.isRequired),
  }).isRequired,
};
export default Navigation;
