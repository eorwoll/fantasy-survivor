import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { css } from '@emotion/core';

const episodesBlock = css`
  margin-bottom: 45px;
`;

/**
 * Renders a list of all episodes in descending order
 */
class AllEpisodes extends React.Component {
  state = {
    episodes: [],
  };

  componentDidMount() {
    let { season_id } = this.props;
    let data = { season_id: season_id };

    axios
      .post('/api/v1/episodes', { data: data })
      .then(response => {
        this.setState({
          episodes: response.data,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  createMarkup(markup) {
    return { __html: markup };
  }

  render() {
    let { episodes } = this.state;

    return (
      <div className="panel-wrapper">
        <h1>Season 40</h1>
        {episodes.map(e => {
          return (
            <div key={e.episode_num} css={episodesBlock}>
              <h2>Episode {e.episode_num}</h2>
              <div dangerouslySetInnerHTML={this.createMarkup(e.points_text)} />
            </div>
          );
        })}
      </div>
    );
  }
}

AllEpisodes.propTypes = {
  /** Current season ID */
  season_id: PropTypes.number.isRequired,
};

export default AllEpisodes;
