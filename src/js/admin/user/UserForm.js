import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../Components';

const { object, func, array } = PropTypes;

/**
 * Form for editing and adding users
 * @param {object} user - user data
 * @returns {string} - user form react component
 */
const UserForm = ({
  user,
  survivors,
  handleChange,
  handleCancel,
  handleSaveNew,
  addingNew,
}) => {
  return (
    <form>
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input
          key="email"
          type="email"
          id="email"
          className="form-control"
          name="email"
          value={user.email}
          onChange={handleChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="fname">First Name</label>
        <input
          key="fname"
          type="text"
          className="form-control"
          name="fname"
          value={user.fname}
          onChange={handleChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="lname">Last Name</label>
        <input
          key="lname"
          type="text"
          className="form-control"
          name="lname"
          value={user.lname}
          onChange={handleChange}
          required
        />
      </div>
      {!addingNew && (
        <>
          <div className="form-group">
            <label htmlFor="survivor1Sel">Survivor 1:</label>
            <br />
            <select
              name="survivor1"
              className="form-control"
              onChange={handleChange}
              defaultValue={user.survivor1_id}
              required
            >
              {survivors.map(s => {
                return (
                  <option value={s.id} key={`s1_${s.id}`}>
                    {s.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="survivor2Sel">Survivor2:</label>
            <br />
            <select
              name="survivor2"
              className="form-control"
              onChange={handleChange}
              defaultValue={user.survivor2_id}
              required
            >
              {survivors.map(s => {
                return (
                  <option value={s.id} key={`s2_${s.id}`}>
                    {s.name}
                  </option>
                );
              })}
            </select>
          </div>
        </>
      )}
      <div className="form-group">
        <Button type="button" onClick={handleSaveNew}>
          Save
        </Button>
        <Button type="button" onClick={handleCancel}>
          Cancel
        </Button>
      </div>
    </form>
  );
};
UserForm.displayName = 'UserForm';
UserForm.propTypes = {
  user: object.isRequired,
  survivors: array.isRequired,
  handleChange: func.isRequired,
  handleCancel: func.isRequired,
};

export default UserForm;
