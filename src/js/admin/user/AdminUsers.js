import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import UserForm from './UserForm';
import { IconButton, Container, Tr, Button } from '../../Components';

const { number } = PropTypes;

class AdminUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      editing: false,
      currentUser: {
        id: null,
        email: '',
        fname: '',
        lname: '',
        survivor1: null,
        survivor1_id: null,
        survivor2: null,
        survivor2_id: null,
      },
      survivors: [],
      addingNew: false,
      error: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSaveNew = this.handleSaveNew.bind(this);
    this.cancel = this.cancel.bind(this);
  }

  componentDidMount() {
    let { season_id } = this.props;

    axios
      .get('/api/v1/allUsers')
      .then(response => {
        this.setState({
          users: response.data,
        });
      })
      .catch(function(error) {
        console.error(error);
      });

    axios
      .post('/api/v1/currentSurvivors', { data: { season_id: season_id } })
      .then(response => {
        this.setState({
          survivors: response.data,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  editUser(user) {
    this.setState({ currentUser: user, editing: true });
  }

  addUser() {
    const currentUser = {
      id: null,
      email: '',
      fname: '',
      lname: '',
      survivor1: null,
      survivor1_id: null,
      survivor2: null,
      survivor2_id: null,
    };

    this.setState({ currentUser: currentUser, editing: true, addingNew: true });
  }

  cancel() {
    this.setState({ editing: false, addingNew: false });
  }

  handleChange(e) {
    const { name, value } = e.target;
    const { currentUser } = this.state;
    let user = currentUser;
    console.log('value', value);

    switch (name) {
      case 'email':
        user.email = value;
        this.setState({ currentUser: user });
        break;
      case 'fname':
        user.fname = value;
        this.setState({ currentUser: user });
        break;
      case 'lname':
        user.lname = value;
        this.setState({ currentUser: user });
        break;
      case 'survivor1':
        user.survivor1_id = value;
        user.survivor1 = this.getSurvivorById(value);
        this.setState({ currentUser: user });
        break;
      case 'survivor2':
        user.survivor2_id = value;
        user.survivor2 = this.getSurvivorById(value);
        this.setState({ currentUser: user });
        break;
      default:
    }
  }

  handleSaveNew() {
    // Save new user
    const { currentUser, users } = this.state;
    let u = [...users];
    axios.post('/api/v1/user/add', { data: currentUser }).then(response => {
      if (response.data.status === 'success') {
        u.push(currentUser);
        this.setState({ users: u, editing: false, addingNew: false });
      } else {
        console.error('Unable to save user');
      }
    });
  }

  getSurvivorById(id) {
    const { survivors } = this.state;
    const selectedSurvivor = survivors.filter(obj => {
      return obj.id === id;
    });
    return selectedSurvivor[0].name;
  }

  render() {
    let { users, editing, currentUser, survivors, addingNew } = this.state;
    let heading = 'Users';

    if (addingNew) heading = 'Add New User';
    else if (editing) heading = 'Edit User';

    let LineItem = ({ user }) => {
      return (
        <Tr>
          <td>{user.fname}</td>
          <td>
            <IconButton icon="pencil" onClick={() => this.editUser(user)} />
          </td>
        </Tr>
      );
    };

    return (
      <div className="panel-wrapper" key="panel-wrapper">
        <Container key="container">
          <h1>{heading}</h1>
          {editing ? (
            <UserForm
              user={currentUser}
              survivors={survivors}
              handleChange={this.handleChange}
              handleCancel={this.cancel}
              handleSaveNew={this.handleSaveNew}
              addingNew={addingNew}
              key="userForm"
            />
          ) : (
            <table>
              <thead>
                <Tr>
                  <th>Name</th>
                  <th>Edit</th>
                </Tr>
              </thead>
              <tbody>
                {users.map(user => {
                  return <LineItem user={user} key={user.id} />;
                })}
              </tbody>
            </table>
          )}
          {!editing && (
            <Button type="button" onClick={() => this.addUser()}>
              Add User
            </Button>
          )}
        </Container>
      </div>
    );
  }
}

AdminUsers.propTypes = {
  season_id: number.isRequired,
};

export default AdminUsers;
