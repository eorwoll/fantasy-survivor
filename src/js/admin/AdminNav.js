import React from 'react';
import { Link } from 'react-router-dom';

const AdminNav = () => {
  return (
    <div className="cms-nav-container">
      <div className="cms-nav-title">Fantasy Survivor CMS</div>
      <ul>
        <li>
          <Link to="/cms/users" />
        </li>
        <li>
          <Link to="/cms/survivors" />
        </li>
        <li>
          <Link to="/cms/tribes" />
        </li>
        <li>
          <Link to="/cms/points" />
        </li>
        <li>
          <Link to="/cms/episode" />
        </li>
      </ul>
    </div>
  );
};

export default AdminNav;
