import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import styled from '@emotion/styled';
import ButterToast, { POS_TOP, POS_CENTER, Cinnamon } from 'butter-toast';
import TribeForm from './TribeForm';
import { PanelWrapper, Container, Button } from '../Components';

const TribeFormContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TribeContainer = styled.div`
  width: 50%;
  margin-bottom: 20px;
`;

const Tribe = styled.div`
  line-height: 40px;
  border-bottom: 1px solid grey;
`;

class AddTribe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      tribe: {
        id: '',
        name: '',
        season_id: props.match.params.season_id,
      },
      savedTribes: [],
    };

    this.change = this.change.bind(this);
    this.save = this.save.bind(this);
  }

  change(e) {
    const { value } = e.target;
    const { tribe } = this.state;
    let t = tribe;

    t.name = value;
    this.setState({ tribe: t });
  }

  save() {
    const { tribe, savedTribes } = this.state;
    const t = { ...tribe };
    let tribes = [...savedTribes];

    axios
      .post('/api/v1/tribes/add', { data: tribe })
      .then(response => {
        t.id = response.data.id;
        tribes.push(t);
        this.setState({ savedTribes: tribes });
        ButterToast.raise({
          content: (
            <Cinnamon.Crunch
              scheme={Cinnamon.Crunch.SCHEME_GREEN}
              content={() => <div>Tribe saved successfully.</div>}
              title="Succuss!"
            />
          ),
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ error: error.message });
      });

    ButterToast.raise({
      content: (
        <Cinnamon.Crunch
          scheme={Cinnamon.Crunch.SCHEME_GREEN}
          content={() => <div>Tribe saved successfully.</div>}
          title="Succuss!"
        />
      ),
    });
  }

  render() {
    const { tribe, savedTribes, error } = this.state;
    const position = {
      vertical: POS_TOP,
      horizontal: POS_CENTER,
    };

    return (
      <PanelWrapper>
        <Container>
          <h1>New Season</h1>
          <TribeFormContainer>
            <div>
              <TribeForm tribe={tribe} change={this.change} />
              <div>
                <Button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.save}
                >
                  Save
                </Button>
                <Button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.next}
                >
                  Next
                </Button>
              </div>
            </div>
            <TribeContainer>
              {savedTribes.map(tribe => {
                return <Tribe key={tribe.id}>{tribe.name}</Tribe>;
              })}
            </TribeContainer>
          </TribeFormContainer>
          <div>{error}</div>
          <ButterToast position={position} />
        </Container>
      </PanelWrapper>
    );
  }
}

export default AddTribe;
