function reducer(state, action) {
  switch (action.type) {
    case 'name':
      return { ...state, name: action.value };
    case 'photo':
      return { ...state, photo: action.value };
    case 'age':
      return { ...state, age: action.value };
    case 'residence':
      return { ...state, residence: action.value };
    case 'occupation':
      return { ...state, occupation: action.value };
    case 'tribe':
      return { ...state, tribe_id: action.value };
    case 'setData':
      return action.value;
    default:
      throw new Error();
  }
}

export default reducer;
