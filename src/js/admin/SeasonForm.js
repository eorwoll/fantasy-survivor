import React from 'react';
import PropTypes from 'prop-types';
import { Input } from '../Components';

const { shape, func, string } = PropTypes;

const SeasonForm = ({ season, change, saveSeason, error }) => {
  return (
    <form>
      <Input
        type="text"
        name="season_num"
        value={season.season_num}
        placeholder="Season Number"
        onChange={change}
      />
      <Input
        type="text"
        name="name"
        value={season.name}
        placeholder="Season Name"
        onChange={change}
      />
      <Input
        type="text"
        name="location"
        value={season.location}
        placeholder="Location"
        onChange={change}
      />
      <div>
        <button type="button" className="btn btn-primary" onClick={saveSeason}>
          Next
        </button>
      </div>
      <div>{error}</div>
    </form>
  );
};

SeasonForm.propTypes = {
  season: shape({
    season_num: string.isRequired,
    name: string.isRequired,
    location: string.isRequired,
  }).isRequired,
  change: func.isRequired,
  saveSeason: func.isRequired,
  error: string.isRequired,
};

export default SeasonForm;
