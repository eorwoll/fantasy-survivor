import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';
import { css } from '@emotion/core';
import { Edit } from '@material-ui/icons';
import { Container, Tr, Button } from '../Components';

class AdminSurvivors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      survivors: [],
    };

    this.votedOff = this.votedOff.bind(this);
  }

  componentDidMount() {
    let { season_id } = this.props;
    let data = { season_id: season_id };

    axios
      .post('/api/v1/currentSurvivors', { data: data })
      .then(response => {
        this.setState({
          survivors: response.data,
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  votedOff(id, votedOff) {
    console.log(id, votedOff);
    const { survivors } = this.state;
    const s = [...survivors];
    const updatedStatus = votedOff === '0' ? '1' : '0';
    const index = s.findIndex(x => x.id === id);
    s[index].voted_off = updatedStatus;

    const data = { id: id, voted_off: updatedStatus };
    axios
      .post('/api/v1/survivors/status', { data: data })
      .then(response => {
        this.setState({ survivors: s });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    let { survivors, editing } = this.state;

    return (
      <div className="panel-wrapper" key="panel-wrapper">
        <Container key="container">
          <h1>{editing ? 'Edit Survivor' : 'Survivors'}</h1>
          <table>
            <thead>
              <Tr>
                <th>Name</th>
                <th>Tribe</th>
                <th
                  css={css`
                    text-align: center;
                  `}
                >
                  Voted Off
                </th>
                <th>Edit</th>
              </Tr>
            </thead>
            <tbody>
              {survivors.map(survivor => {
                return (
                  <Tr key={survivor.id}>
                    <td>{survivor.name}</td>
                    <td>{survivor.tribe}</td>
                    <td align="center">
                      <input
                        type="checkbox"
                        name={survivor.id}
                        defaultChecked={
                          survivor.voted_off === '1' ? 'true' : ''
                        }
                        onClick={() =>
                          this.votedOff(survivor.id, survivor.voted_off)
                        }
                      />
                    </td>
                    <td>
                      <Link to={`/cms/survivors/edit/?id=${survivor.id}`}>
                        <Edit
                          css={css`
                            cursor: pointer;
                          `}
                        />
                      </Link>
                    </td>
                  </Tr>
                );
              })}
            </tbody>
          </table>
        </Container>
      </div>
    );
  }
}

AdminSurvivors.propTypes = {
  /** Current season ID */
  season_id: PropTypes.number.isRequired,
};
export default AdminSurvivors;
