import React, { useState, useEffect, useReducer } from 'react';
import { number } from 'prop-types';
import axios from 'axios';
import { css } from '@emotion/core';
import { PanelWrapper, Container, Button, Input, Select } from '../Components';
import { getQueryStringParams } from '../utils/utils';
import reducer from './reducers/survivorsReducer';

const initialState = {
  name: '',
  photo: '',
  age: '',
  residence: '',
  occupation: '',
  tribe_id: '',
};

function EditSurvivor({ season_id }) {
  const { id } = getQueryStringParams(window.location.search);
  const [tribes, setTribes] = useState(null);
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const data = { seasonId: season_id };
    axios
      .post('/api/v1/currentTribes', { data: data })
      .then(response => {
        setTribes(response.data);
        axios
          .post('/api/v1/survivor/get', { data: id })
          .then(response => {
            dispatch({ type: 'setData', value: response.data });
          })
          .catch(error => {
            console.error(error);
          });
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  const save = e => {
    e.preventDefault();
    axios
      .put('/api/v1/survivors/edit', { data: state })
      .then(response => {
        if (response.data.status === 'success')
          window.location.href = '/cms/survivors';
      })
      .catch(e => {
        alert('Oops! An error occured. ' + e);
      });
  };

  const handleChange = e => {
    dispatch({ type: e.target.name, value: e.target.value });
  };

  return (
    <PanelWrapper>
      <Container size="sm" key="container">
        <form
          onSubmit={save}
          css={css`
            margin-top: 20px;
          `}
        >
          <div>
            <div>
              <label>Name</label>
            </div>
            <Input
              type="text"
              name="name"
              value={state.name}
              size="40"
              onChange={handleChange}
            />
          </div>
          <div>
            <div>
              <label>Photo</label>
            </div>
            <Input
              type="text"
              name="photo"
              value={state.photo}
              size="40"
              onChange={handleChange}
            />
          </div>
          <div>
            <div>
              <label>Age</label>
            </div>
            <Input
              type="text"
              name="age"
              value={state.age}
              size="40"
              onChange={handleChange}
            />
          </div>
          <div>
            <div>
              <label>Residence</label>
            </div>
            <Input
              type="text"
              name="residence"
              value={state.residence}
              size="40"
              onChange={handleChange}
            />
          </div>
          <div>
            <div>
              <label>Occupation</label>
            </div>
            <Input
              type="text"
              name="occupation"
              value={state.occupation}
              size="40"
              onChange={handleChange}
            />
          </div>
          <div>
            <div>
              <label>Tribe</label>
            </div>
            <div>
              <Select
                name="tribe"
                selected={state.tribe_id}
                values={tribes}
                handleChange={handleChange}
              />
            </div>
          </div>
          <div>
            <Button type="submit">Save</Button>
          </div>
        </form>
      </Container>
    </PanelWrapper>
  );
}

EditSurvivor.propTypes = {
  season_id: number.isRequired,
};

export default EditSurvivor;
