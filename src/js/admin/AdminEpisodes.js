import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { css } from 'emotion';
import { Link } from 'react-router-dom';
import { PanelWrapper, Container, Tr, Button } from '../Components';

class AdminEpisodes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      episodes: [],
    };
  }

  componentDidMount() {
    const { season_id } = this.props;
    const data = { season_id: season_id };

    axios
      .post('/api/v1/episodes', { data: data })
      .then(response => {
        this.setState({ episodes: response.data });
      })
      .catch(error => {
        console.error(error);
      });
  }

  createMarkup(markup) {
    return { __html: markup };
  }

  render() {
    const { season_id } = this.props;
    const { episodes } = this.state;

    return (
      <PanelWrapper>
        <Container>
          <h1>Episodes</h1>
          <table
            className={css`
              margin-bottom: 16px;
            `}
          >
            <thead>
              <Tr>
                <th>Episode</th>
                <th>Text</th>
              </Tr>
            </thead>
            <tbody>
              {episodes.map(episode => {
                return (
                  <Tr key={episode.id}>
                    <td
                      className={css`
                        width: 100px;
                        text-align: center;
                      `}
                    >
                      {episode.episode_num}
                    </td>
                    <td
                      dangerouslySetInnerHTML={this.createMarkup(
                        episode.points_text
                      )}
                    />
                  </Tr>
                );
              })}
            </tbody>
          </table>
          <Link
            to={`/cms/add-episode/${season_id}`}
            className="btn btn-primary"
          >
            Add Episode
          </Link>
        </Container>
      </PanelWrapper>
    );
  }
}

AdminEpisodes.propTypes = {
  season_id: PropTypes.number.isRequired,
};

export default AdminEpisodes;
