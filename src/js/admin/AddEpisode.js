import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import axios from 'axios';
import { Editor } from '@tinymce/tinymce-react';
import ButterToast, { POS_TOP, POS_CENTER, Cinnamon } from 'butter-toast';
import Toast from '../Toast';

import { PanelWrapper, Container, Input, Tr, Button } from '../Components';

const { object, func, shape, string } = PropTypes;

export const PointsInput = styled.input`
  height: 30px;
`;

const LineItem = ({ survivor, changePoints }) => {
  return (
    <Tr>
      <td
        className={css`
          width: 300px;
        `}
      >
        {survivor.name}
      </td>
      <td>{survivor.tribe}</td>
      <td>
        <PointsInput
          type="text"
          name={survivor.id}
          value={survivor.ammount}
          onChange={changePoints}
          maxLength="2"
          size="2"
        />
      </td>
    </Tr>
  );
};
LineItem.propTypes = {
  survivor: object.isRequired,
  changePoints: func.isRequired,
};

class AddEpisode extends Component {
  constructor(props) {
    super(props);

    this.state = {
      episode: {
        episode_num: '',
        season_id: props.match.params.season_id,
        points_text: '',
      },
      survivors: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.changePoints = this.changePoints.bind(this);
    this.save = this.save.bind(this);
  }

  componentDidMount() {
    const {
      match: {
        params: { season_id },
      },
    } = this.props;
    const data = { season_id: season_id };

    axios
      .post('/api/v1/activeSurvivors', { data: data })
      .then(response => {
        response.data.forEach(survivor => {
          survivor.amount = 0;
        });
        this.setState({
          survivors: response.data,
        });
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  handleChange(e) {
    const { value, name } = e.target;
    const { episode } = this.state;
    const ep = { ...episode };

    ep[name] = value;
    this.setState({ episode: ep });
  }

  handleEditorChange(e) {
    const { episode } = this.state;
    const ep = { ...episode };

    ep.points_text = e.target.getContent();
    this.setState({ episode: ep });
  }

  changePoints(e) {
    const { survivors } = this.state;
    const { value, name } = e.target;
    const s = [...survivors];
    const index = s.findIndex(x => x.id === name);
    s[index].amount = value;
    this.setState({ survivors: s });
  }

  save() {
    axios
      .post('/api/v1/episode/add', { data: this.state })
      .then(response => {
        ButterToast.raise({
          content: (
            <Cinnamon.Crunch
              scheme={Cinnamon.Crunch.SCHEME_GREEN}
              content={() => <div>Episode saved successfully.</div>}
              title="Succuss!"
            />
          ),
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const {
      episode: { episode_num, points_text },
      survivors,
    } = this.state;

    const position = {
      vertical: POS_TOP,
      horizontal: POS_CENTER,
    };

    return (
      <PanelWrapper>
        <Container>
          <h1>Add Episode</h1>
          <form>
            <Input
              type="text"
              name="episode_num"
              value={episode_num}
              onChange={this.handleChange}
              placeholder="Episode Number"
            />
            <div className="form-group">
              <label>
                Episode Description:
                <Editor
                  initialValue={points_text}
                  init={{
                    plugins: 'link image code',
                    toolbar:
                      'undo redo | bold italic | alignleft aligncenter alignright | code',
                    width: 500,
                    height: 200,
                  }}
                  onChange={this.handleEditorChange}
                />
              </label>
            </div>
            <div
              className={css`
                margin-bottom: 16px;
              `}
            >
              <table>
                <thead>
                  <tr
                    className={css`
                      border-bottom: 1px solid grey;
                      text-transform: uppercase;
                    `}
                  >
                    <td>Name</td>
                    <td
                      className={css`
                        width: 130px;
                      `}
                    >
                      Tribe
                    </td>
                    <td>Points</td>
                  </tr>
                </thead>
                <tbody>
                  {survivors.map(survivor => {
                    return (
                      <LineItem
                        survivor={survivor}
                        key={survivor.id}
                        changePoints={this.changePoints}
                      />
                    );
                  })}
                </tbody>
              </table>
            </div>
            <Button
              type="button"
              className="btn btn-primary"
              onClick={this.save}
            >
              Save
            </Button>
          </form>
          <ButterToast position={position} />
          <Toast />
        </Container>
      </PanelWrapper>
    );
  }
}
AddEpisode.propTypes = {
  match: shape({
    params: shape({
      season_id: string.isRequired,
    }).isRequired,
  }).isRequired,
};
export default AddEpisode;
