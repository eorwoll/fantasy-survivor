import React, { Component } from 'react';
import axios from 'axios';
import propTypes from 'prop-types';
import { Edit, AddCircle } from '@material-ui/icons';
import { css } from 'emotion';

import { PanelWrapper, Container, Input, Button, Tr } from '../Components';

const { number } = propTypes;

class AdminTribes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tribes: [],
      adding: false,
      newTribe: '',
    };

    this.change = this.change.bind(this);
    this.addTribe = this.addTribe.bind(this);
  }

  componentDidMount() {
    console.log('component mounted');

    let { season_id } = this.props;
    let data = { seasonId: season_id };

    axios
      .post('/api/v1/currentTribes', { data: data })
      .then(response => {
        this.setState({
          tribes: response.data,
        });
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  editTribe(tribe) {
    console.log('tribe', tribe);
  }

  change(e) {
    const { value } = e.target;

    this.setState({ newTribe: value });
  }

  addTribe() {
    const { newTribe, tribes } = this.state;
    const currentTribes = [...tribes];
    const { season_id } = this.props;
    const data = { season_id: season_id, name: newTribe };

    axios
      .post('/api/v1/tribes/add', { data: data })
      .then(response => {
        currentTribes.push({ id: response.data.id, name: newTribe });
        this.setState({ tribes: currentTribes, adding: false, newTribe: '' });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const { tribes, adding, newTribe } = this.state;

    return (
      <PanelWrapper>
        <Container size="sm">
          <h1>Tribes</h1>
          <table>
            <thead>
              <Tr>
                <th colSpan="2">Tribe</th>
              </Tr>
            </thead>
            <tbody>
              {tribes.map(tribe => {
                return (
                  <Tr key={tribe.id}>
                    <td>{tribe.name}</td>
                    <td align="right">
                      <Edit
                        css={css`
                          cursor: pointer;
                        `}
                        onClick={() => this.editTribe(tribe)}
                      />
                    </td>
                  </Tr>
                );
              })}
              {adding && (
                <Tr>
                  <td>
                    <Input
                      onChange={this.change}
                      value={newTribe}
                      className={css`
                        margin-bottom: 0;
                      `}
                    />
                  </td>
                  <td align="right">
                    <AddCircle onClick={this.addTribe} />
                  </td>
                </Tr>
              )}
            </tbody>
          </table>
          {!adding && (
            <Button onClick={() => this.setState({ adding: true })}>
              Add Tribe
            </Button>
          )}
        </Container>
      </PanelWrapper>
    );
  }
}

AdminTribes.propTypes = {
  season_id: number.isRequired,
};

export default AdminTribes;
