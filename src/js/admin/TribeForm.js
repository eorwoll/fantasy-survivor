import React from 'react';
import PropTypes from 'prop-types';
import { Input } from '../Components';

const { shape, func, string } = PropTypes;

const TribeForm = ({ tribe, change }) => {
  return (
    <form>
      <Input
        type="text"
        name="name"
        value={tribe.name}
        placeholder="Tribe Name"
        onChange={change}
      />
    </form>
  );
};

TribeForm.propTypes = {
  tribe: shape({
    name: string.isRequired,
  }).isRequired,
  change: func.isRequired,
};

export default TribeForm;
