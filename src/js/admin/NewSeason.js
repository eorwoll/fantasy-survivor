import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import SeasonForm from './SeasonForm';
import { PanelWrapper, Container } from '../Components';

class NewSeason extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      season: {
        id: '',
        season_num: '',
        name: '',
        location: '',
      },
    };

    this.change = this.change.bind(this);
    this.saveSeason = this.saveSeason.bind(this);
  }

  change(e) {
    const { name, value } = e.target;
    const { season } = this.state;
    let s = season;

    switch (name) {
      case 'season_num':
        s.season_num = value;
        this.setState({ season: s });
        break;
      case 'name':
        s.name = value;
        this.setState({ season: s });
        break;
      case 'location':
        s.location = value;
        this.setState({ season: s });
        break;
      default:
    }
  }

  saveSeason(e) {
    e.preventDefault;

    const { season } = this.state;
    let newSeason = season;

    axios
      .post('/api/v1/newSeason', { data: season })
      .then(response => {
        newSeason.id = response.data.id;
        window.location.href = `/cms/add-tribe/${newSeason.id}`;
      })
      .catch(error => {
        console.error(error);
        this.setState({ error: error.message });
      });
  }

  render() {
    const { season, tribes, error, tribeError, step } = this.state;

    return (
      <PanelWrapper>
        <Container>
          <h1>New Season</h1>
          <SeasonForm
            season={season}
            change={this.change}
            saveSeason={this.saveSeason}
            error={error}
          />
        </Container>
      </PanelWrapper>
    );
  }
}

export default NewSeason;
