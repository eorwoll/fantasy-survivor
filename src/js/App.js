import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import Navigation from './Nav';
import Episodes from './Episodes';
import Standings from './Standings';
import Survivors from './Survivors';
import Rules from './Rules';
import AdminSurvivors from './admin/AdminSurvivors';
import AdminUsers from './admin/user/AdminUsers';
import NewSeason from './admin/NewSeason';
import AdminTribes from './admin/AdminTribes';
import AddTribe from './admin/AddTribe';
import AddEpisode from './admin/AddEpisode';
import AdminEpisodes from './admin/AdminEpisodes';
import EditSurvivor from './admin/EditSurvivor';

import '../css/app.scss';

class App extends React.Component {
  state = {
    seasonId: 10,
    loading: true,
  };

  componentDidMount() {
    axios.get('/api/v1/latest-season').then(response => {
      this.setState({
        seasonId: parseInt(response.data.id.id),
        loading: false,
      });
    });
  }

  render() {
    const { seasonId, loading } = this.state;
    const { pathname } = window.location;
    const isCMS = pathname.includes('cms');

    const nav = isCMS
      ? {
          title: 'Fantasy Survivor CMS',
          backgroundColor: '#6d7977',
          items: [
            {
              url: '/cms/users',
              name: 'Users',
            },
            {
              url: '/cms/survivors',
              name: 'Survivors',
            },
            {
              url: '/cms/tribes',
              name: 'Tribes',
            },
            {
              url: `/cms/add-episode/${seasonId}`,
              name: 'Add Episode',
            },
            {
              url: '/cms/new-season',
              name: 'Create New Season',
            },
          ],
        }
      : {
          title: 'Fantasy Survivor',
          backgroundColor: 'none',
          items: [
            {
              url: '/standings',
              name: 'Standings',
            },
            {
              url: '/episodes',
              name: 'Episodes',
            },
            {
              url: '/survivors',
              name: 'Survivors',
            },
            {
              url: '/rules',
              name: 'Rules',
            },
          ],
        };

    return (
      <Router>
        <div>
          {!loading && (
            <>
              <Navigation nav={nav} />
              <Switch>
                <Route
                  exact
                  path="/"
                  component={() => <Standings season_id={seasonId} />}
                />
                <Route
                  path="/standings"
                  component={() => <Standings season_id={seasonId} />}
                />
                <Route
                  path="/episodes"
                  component={() => <Episodes season_id={seasonId} />}
                />
                <Route
                  path="/survivors"
                  component={() => <Survivors season_id={seasonId} />}
                />
                <Route path="/rules" component={Rules} />
                <Route
                  exact
                  path="/cms"
                  component={() => <AdminUsers season_id={seasonId} />}
                />
                <Route
                  exact
                  path="/cms/survivors"
                  component={() => <AdminSurvivors season_id={seasonId} />}
                />
                <Route
                  exact
                  path="/cms/survivors/edit"
                  component={() => <EditSurvivor season_id={seasonId} />}
                />
                <Route
                  exact
                  path="/cms/new-season"
                  component={() => <NewSeason />}
                />
                <Route
                  exact
                  path="/cms/tribes"
                  component={() => <AdminTribes season_id={seasonId} />}
                />
                <Route
                  exact
                  path="/cms/add-tribe/:season_id"
                  component={AddTribe}
                />
                <Route
                  exact
                  path="/cms/episodes"
                  component={() => <AdminEpisodes season_id={seasonId} />}
                />
                <Route
                  exact
                  path="/cms/add-episode/:season_id"
                  component={AddEpisode}
                />
                <Route component={NotFound} />
              </Switch>
            </>
          )}
        </div>
      </Router>
    );
  }
}

const NotFound = () => <h1>404.. This page is not found!</h1>;

export default App;
