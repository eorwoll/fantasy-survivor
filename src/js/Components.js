import React from 'react';
import { css } from '@emotion/core';
import PropTypes, { object } from 'prop-types';
import styled from '@emotion/styled';

const { string, func } = PropTypes;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  ${props => (props.size === 'sm' ? 'margin: 0 15%' : '')}
`;

export const Tr = styled.tr`
  height: 40px;
  border-bottom: 1px solid #999;
`;

export const IconButton = ({ icon, onClick }) => {
  const styles = css`
    border: transparent;
    cursor: pointer;
  `;

  return (
    <button type="button" css={styles} onClick={onClick}>
      <i className={`fa fa-${icon}`} />
    </button>
  );
};

IconButton.propTypes = {
  icon: string.isRequired,
  onClick: func.isRequired,
};

export const PanelWrapper = styled.div`
  max-width: 970px;
  margin: 0 auto;
`;

export const Input = styled.input`
  height: 40px;
  margin: 4px 20px 20px 0px;
  padding: 6px;
`;

export const Select = ({ name, selected, values, handleChange }) => {
  return (
    <select name={name} value={selected} onChange={handleChange}>
      {values &&
        values.map(v => {
          return (
            <option key={v.id} value={v.id}>
              {v.name}
            </option>
          );
        })}
    </select>
  );
};
Select.propTypes = {
  name: string.isRequired,
  selected: string.isRequired,
  values: object.isRequired,
  handleChange: func.isRequired,
};

export const Button = styled.button`
  margin: 16px 16px 16px 0;
  padding: 6px 16px;
  background-color: #2996be;
  color: #fff;
  border-color: #2996be;
  width: fit-content;
  cursor: pointer;

  &:hover {
    background-color: #86c1c5;
    border-color: #86c1c5;
  }
`;
