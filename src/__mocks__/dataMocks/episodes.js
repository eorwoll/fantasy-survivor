export default [
  {
    id: '135',
    episode_num: '3',
    season_id: '8',
    points_text:
      '<p>Keith stays in the game and goes to Edge of Extinction!</p>\n<p>Manu win tribe reward challenge: Dan, Rick, Wendy, Lauren, Chris, Kelly, and David each earn&nbsp;<strong>1</strong> point.</p>\n<p>Aubry finds hidden immunity idol.&nbsp;<strong>2&nbsp;</strong>points.</p>\n<p>Kama win tribe immunity challenge: Joe, Aubry, Victoria, Ron, Julia, Eric, Aurora, Julie, and Gavin each earn&nbsp;<strong>2&nbsp;</strong>points.</p>\n<p>Chris voted off and decides to go to Edge of Extinction.</p>',
  },
  {
    id: '134',
    episode_num: '2',
    season_id: '8',
    points_text:
      '<p>Lauren finds hidden immunity idol. <strong>2</strong> points.</p>\n<p>Kama win tribe immunity challenge. Joe, Aubry, Victoria, Ron, Julia, Eric, Aurora, Julie, and Gavin each earn&nbsp;<strong>2</strong> points.</p>\n<p>Keith voted off but does he decide to go to Exile Island? Stay tuned for next episode!</p>',
  },
  {
    id: '133',
    episode_num: '1',
    season_id: '8',
    points_text:
      '<p>Kama win tribe immunity challenge. Joe, Aubry, Victoria, Ron, Julia, Eric, Aurora, Julie, and Gavin earn 2 points each.</p>\n<p>Reem voted off but earns a choice to stay in the game.</p>',
  },
];
